UVOS UsbLedController
==============

This is the firmware for the usbled controller pcb  
license: GPL v3.0 with parts optionally under LGPL v3.0

Building and Installing
----------

Requirements:

* cmake
* avr-gcc
* avr-libc
* avrdude
* a usbasp device

Install instructions:

1. run 'cmake .'
2. if programming a new pcb run 'make fuses'
3. run 'make export'
4. run 'make download'
