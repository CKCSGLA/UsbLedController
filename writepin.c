/*UVOS*/

/* This file is part of UsbLedController.
 *
 * TelemetrySystem is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License (LGPL) version 3 as published by
 * the Free Software Foundation.
 *
 * TelemetrySystem is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TelemetrySystem.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "writepin.h"

void writePin(volatile unsigned char * const port, const unsigned char pin, const bool state)
{
	*port = (*port & ~(1 << pin)) | (1 << pin)*state;
}

void setBit( volatile unsigned char * const reg, const unsigned char bit, const bool value )
{
    writePin(reg, bit, value);
}

void setDirection( volatile unsigned char * const portDirReg, const unsigned char pin, const bool makeOutput )
{
    writePin(portDirReg, pin, makeOutput);
}

bool readPin( volatile const unsigned char * const inPort, const unsigned char pin)
{ 
	return (bool) (*inPort & (1 << pin));
}
